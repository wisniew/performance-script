#!/bin/bash

# Performance Script v1
# 
# Script for change governor to performance

#    Copyright 2018 Rafal Wisniewski (wisniew99@gmail.com)
#    Copyright 2018 Jakub Wozniak (kuba.wn24@gmail.com)
#    Copyright 2018 Norbert Mikula (albahimpl@gmail.com)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


# Colors
red=$(tput setaf 1)  # Red
grn=$(tput setaf 2)  # Green
txtrst=$(tput sgr0)  # Reset
pur=$(tput setaf 201) # Purple

echo -e '\0033\0143'
if [[ ! $EUID -ne 0 ]]
then
     x=0
     JOBS="$(grep -c "processor" "/proc/cpuinfo")"
     ((JOBS--))

     cpu=$(cat /sys/bus/cpu/devices/cpu$x/cpufreq/scaling_governor)

     if [ ! $cpu == "performance" ]
     then
          echo "Changing to performance..."
          while [ $x -le $JOBS ]
          do
               echo "CPU:$x"
               echo "Current governor:"
               cat /sys/bus/cpu/devices/cpu$x/cpufreq/scaling_governor
               echo "Setting to performance cpu$x..."
               echo "performance" > /sys/bus/cpu/devices/cpu$x/cpufreq/scaling_governor
               ((x++))
          done
     else
          echo "${grn}Performance already set!${txtrst}"
          echo ""
          echo "${pur}Script finished all the tasks!${txtrst}"
          echo ""
          exit 0
     fi

     ((x--))

     cpu=$(cat /sys/bus/cpu/devices/cpu$x/cpufreq/scaling_governor)

     if [ $cpu == "performance" ]
     then
          echo "${grn}Script done all jobs!${txtrst}"
     else
          echo "${red}Something goes wrong!${txtrst}"
     fi
     echo ""
else
     echo "${red}You need ROOT premissions!${txtrst}"
     echo ""
     echo "${pur}Script finished all the tasks!${txtrst}"
     echo ""
fi
